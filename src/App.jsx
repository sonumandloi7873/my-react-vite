import React from 'react'
import "./App.css"
import Event from './react/Event'
import Count from './react/Count'
import ShowHide from './react/ShowHide'
import Rendering from './react/Rendering'
import Mount from './react/lifeCycle.jsx/Mount'
import FnLifeCycle from './react/lifeCycle.jsx/FnLifeCycle'
import { Styles } from './react/Style'
import Map from './react/Map'
import Tables from './react/Table'
import {AddDataInArray} from './react/AddDataInArray'
import Lifting from './react/Lifting'
import {RefUse} from './react/RefUse'
import Hoc from './Hoc'
import Input from './react/input'
import Router from './assets/router'
import Partical from './react/Partical'
import ListArray from './react/ListArray'
import useDataApi from './redux/useDataApi'
import Hello from './Hello'
import HomeE from './component/HomeE'
import Calculator from './Calcuiator/Calculator'
import MemoUse from './MemoUse'
import Context from './ContextUse/Context'
import Weather from './project/Weather'
import Music from './project/Music/Music'

function App() {  

  useDataApi()

  return (
    <>
     {/* <Hello/> */}
      {/* <Event/> */}
      {/* <Count/> */}
      {/* <ShowHide/> */}
      {/* <Rendering/> */}
      {/* <Mount/> */}
      {/* <FnLifeCycle/> */}
      {/* <Styles/> */}
      {/* <Map/> */}
      {/* <Tables/> */}
      {/* <AddDataInArray/> */}
      {/* <Lifting/> */}
      {/* <RefUse/> */}
      {/* <Hoc/> */}
      {/* <Input/> */}
      {/* <Router/> */}
      {/* <Partical/> */}
      {/* <ListArray/> */}
      {/* <HomeE/> */}
      {/* <Calculator/> */}
      {/* <MemoUse/> */}
      {/* <Context/> */}
      {/* <Weather/> */}
      <Music/>
      

    </>
  )
}

export default App
