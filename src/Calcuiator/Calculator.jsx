import React, { useState } from 'react'

function Calculator() {

  const [values,setValues] = useState("")

  const inputstyle = "border-none w-[60px] has-[60px] text-lg bg-[rgb(91,91,151)] m-[2px] rounded-[10px] text-white font-bold cursor-pointer outline-0 h-[50px] hover:bg-[rgb(137,22,245)]"

  return (
    <div className='w-[100%] h-[100vh] flex items-center justify-center bg-gradient-to-r from-[rgb(36,36,223)] to-[rgb(7,7,166)]'>
      <div className='p-[20px] rounded-xl bg-white'>
          <form action="">
            <div className='flex justify-end mt-1 mb-4'>
              <input className={`${inputstyle} text-right flex-1 text-4xl py-[5px] px-[10px] bg-[rgb(64,64,64)] hover:bg-[rgb(137,22,145)]`} type="text"  value={values}/>
            </div>
            <div>
              <input  className={`${inputstyle}`} type="button" value="AC" onClick={()=>setValues("")}/>
              <input   className={`${inputstyle}`} type="button" value="DE" onClick={()=>setValues(values.slice(0,-1))}/>
              <input   className={`${inputstyle}  `} type="button" value="." onClick={(e)=> setValues(values + e.target.value)}/>
              <input   className={`${inputstyle}  `} type="button" value="/" onClick={(e)=> setValues(values + e.target.value)} />
            </div>
            <div>
              <input   className={`${inputstyle} `} type="button" value="7" onClick={(e)=> setValues(values + e.target.value)}/>
              <input   className={`${inputstyle} `} type="button" value="8" onClick={(e)=> setValues(values + e.target.value)}/>
              <input   className={`${inputstyle} `} type="button" value="9" onClick={(e)=> setValues(values + e.target.value)}/>
              <input   className={`${inputstyle} `} type="button" value="*" onClick={(e)=> setValues(values + e.target.value)}/>
            </div>
            <div>
              <input   className={`${inputstyle} `} type="button" value="4" onClick={(e)=> setValues(values + e.target.value)}/>
              <input   className={`${inputstyle} `} type="button" value="5" onClick={(e)=> setValues(values + e.target.value)}/>
              <input   className={`${inputstyle} `} type="button" value="6"  onClick={(e)=> setValues(values + e.target.value)}/>
              <input   className={`${inputstyle} `} type="button" value="+" onClick={(e)=> setValues(values + e.target.value)} />
            </div>
            <div>
              <input   className={`${inputstyle} `} type="button" value="1"onClick={(e)=> setValues(values + e.target.value)}/>
              <input   className={`${inputstyle} `} type="button" value="2"onClick={(e)=> setValues(values + e.target.value)}/>
              <input   className={`${inputstyle} `} type="button" value="3"onClick={(e)=> setValues(values + e.target.value)} />
              <input   className={`${inputstyle} `} type="button" value="-" onClick={(e)=> setValues(values + e.target.value)}/>
            </div>
            <div>
              <input  className={`${inputstyle} `} type="button" value="00" onClick={(e)=> setValues(values + e.target.value)}/>
              <input  className={`${inputstyle} `} type="button" value="0" onClick={(e)=> setValues(values + e.target.value)}/>
              <input className={`${inputstyle} w-[130px]`} type="button" value="=" onClick={()=>setValues(eval(values))} />
            </div>
          </form>
      </div>
    </div>
  )
}

export default Calculator
