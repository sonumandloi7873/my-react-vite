import React, { useContext } from 'react'
import ChildC from './ChildC'
import { data,data1 } from './Context'

function ChildB() {

  const name = useContext(data)
  const age = useContext(data1)

  return (
    <>
      <h3>{name}</h3>
      <h3>{age}</h3>
      <h3>usecontext</h3>
      <ChildC/>

    </>
  )
}

export default ChildB
