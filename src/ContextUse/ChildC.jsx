import React from 'react'
import { data, data1 } from './Context'

function ChildC() {

  return (
    <div>
      <data.Consumer>
        {
          (name) => {
            return (
              <>
                <data1.Consumer>
                  {

                    (age) => {
                      return (
                        <>
                          <h2>{name}</h2>
                          <h2>{age}</h2>
                        </>
                      )
                    }
                  }

                </data1.Consumer>
                <h2>{name}</h2>

              </>

            )
          }
        }

      </data.Consumer>

      context api

    </div>
  )
}

export default ChildC
