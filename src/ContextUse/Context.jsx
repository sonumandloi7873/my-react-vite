import React, { createContext } from 'react'
import ChildA from './ChildA'

const data = createContext()
const data1 = createContext()

function Context() {

  const name = "sonu"
  const age = 55


  return (
    <data.Provider value={name}>
      <data1.Provider  value={age}>
        <ChildA/>
      </data1.Provider>
    </data.Provider>
  )
}

export default Context
export {data,data1}
