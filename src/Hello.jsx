import React from 'react'
import { useSelector } from 'react-redux'

function Hello(){

  const item = useSelector((state)=>state.data.apiList)

console.log("item",item)

  return (
   <>
    <div>
      <h1 className='text-3xl text-red-600 bg-blue-700'>my name is </h1>
      {
        item.map((hi)=>{
          return(
            <>
            
            <h1>{hi.title}</h1>
            <img src={hi.image} alt="" className='w-[200px]' />
            </>
          )
        })
      }
    </div>
   </>
  )
}



export default Hello
