import React, { useState } from 'react'

function Hoc() {
  return (
    <div>
      <h2>HOC</h2>
      <HocRed Counter={Counter} color={"red"}/>
      <HocRed Counter={Counter} color={"green"}/>
      <HocGreen Counter={Counter} color={"blue"}/>
    </div>
  )
}

export default Hoc

function HocRed(props){
  return(
    <div>
      <h2 style={{backgroundColor:props.color}}><props.Counter  name="sonu"/></h2>
    </div>
  )
}
function HocGreen(props){
  return(
    <div>
      <h2 style={{backgroundColor:props.color}}><props.Counter  name="sovjlsdnvklmu"/></h2>
    </div>
  )
}

function Counter(props){


  return(

    <div >my nzme {props.name}</div>
  )
}