import React, { useCallback, useMemo, useState } from 'react'
import { Button } from 'react-bootstrap'

function MemoUse() {

  const [add,setAdd] = useState(0)
  const [minu,setMinu] = useState(100)


const mul  =  useCallback(
  function multi(){
    console.log("sonu")
    return add * 2
  },[add]
)


console.log("a")
setTimeout(()=>{
  console.log("b")
},[1000])
// console.log(new Promise.resolve(5))
console.log("c")

  return (
    <div>
       {mul()}
      <h2>{add}</h2>
      <h2>{minu}</h2>


      <Button onClick={()=>setAdd(add  + 1)}>add</Button> <br />
      <Button onClick={()=>setMinu(minu - 1)}>minu</Button>


      
    </div>
  )
}

export default MemoUse
