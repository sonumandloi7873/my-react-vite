import React from 'react'
import { Nav } from 'react-bootstrap'
import { NavLink } from 'react-router-dom'


function NavBar() {
  return (
   <Nav className='nav-bar'>
    <ul>
      <li><NavLink to="/">Home</NavLink></li>
      <li><NavLink to="/about">about</NavLink></li>
      <li><NavLink to="/gallery">gallery</NavLink></li>
    </ul>
   </Nav>
  )
}

export default NavBar
