import React from 'react'
import { Link } from 'react-router-dom'

function NavBat() {
  return (
    <div>
      <nav>
        <ul>
          <li><Link to="/Contact/a">a</Link></li>
          <li><Link to="/Contact/b">b</Link></li>
          <li><Link to="/Contact/c">c</Link></li>
        </ul>
      </nav>
      
    </div>
  )
}

export default NavBat
