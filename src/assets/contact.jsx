import React from 'react'
import { useParams } from 'react-router-dom'

function Contact() {

  const param = useParams()
  console.log(param.name)

     const { name }  = param


  return (
    <div>
        <h2>my name is {name}</h2>      
    </div>
  )
}

export default Contact
