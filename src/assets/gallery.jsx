import React, { useState } from 'react'
import { NavLink, Outlet } from 'react-router-dom'

function Gallery() {

  const [data,setData] =useState("sonu")

  return (
    <div>
      <ul>
        <li>
          <NavLink to="A" state={data}>A</NavLink><br />
          <NavLink to="B">B</NavLink><br />
          <NavLink to="C">C</NavLink>

          <Outlet/>
        </li>
      </ul>

    </div>
  )
}

export default Gallery
