import React from 'react'
import { Button } from 'react-bootstrap'
import { Link, NavLink, useNavigate, useSearchParams } from 'react-router-dom'


function Home() {

  const naviagte = useNavigate()

  function handlerUrl(a){
    naviagte(a)
  }

  return (
    <div>
      <h1>home page</h1>
      <Link to="/about" className='rout'>go to about</Link> <br />
      <NavLink to="/about" className='rout'>go abot</NavLink> <br />
      <a href="/about">about</a>
      <Button onClick={()=>handlerUrl("/about")}>go about</Button>
      <Button onClick={()=>handlerUrl("/gallery")}>go gallery</Button>

      
    </div>
  )
}

export default Home
