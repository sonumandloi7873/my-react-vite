import React from 'react'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import Home from './home'
import About from './about'
import Page404 from './page404'
import Contact from './contact'
import NavBat from './NavBat'
import Gallery from './gallery'
import NavBar from './NavBar'
import './styleRouter.css'

import HiA from './A'
import HiB from './B'
import HiC from './C'
import HomeE from '../component/HomeE'
import AddItem from '../component/AddItem'
import Deatiles from '../component/Deatiles'

function Router() {
  return (
    <>
    <BrowserRouter>
    {/* <NavBar/> */}
    {/* <NavBat/> */}
      <Routes>
        <Route path='/' element={<HomeE/>}/>
        <Route path='/AddItem' element={<AddItem/>}/>
        <Route path='/deatiles/:cart' element={<Deatiles/>}/>
        {/* <Route  path='/' element={<Home/>}/>
        <Route  path='/about' element={<About/>}/>
        <Route  path='/Contact/:name' element={<Contact/>} />
        <Route  path='/gallery/' element={<Gallery />} >
        <Route  path='A' element={<HiA/>}/>
        <Route  path='B' element={<HiB/>}/>
        <Route  path='C' element={<HiC/>}/>
        </Route>
        <Route  path='/*' element={<Page404/>} /> */}
      </Routes>
    </BrowserRouter>
          
    </>
  )
}

export default Router
