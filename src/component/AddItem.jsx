import React from 'react'
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import { useDispatch, useSelector } from 'react-redux'
import { addToCart, decrement, increment, removeToCart } from '../redux/slice';

function AddItem() {

  const dispatch = useDispatch()
  const items = useSelector((state) => state.data.addCartList)

  const totalItem = items.reduce((total,item)=>total+item.qty,0)
  const totalPrice = items.reduce((total,item)=>total+item.qty * item.item.price,0)

  console.log("totalItem", totalItem)
  console.log("totalPrice", totalPrice)
  // console.log("item", items.item)

  return (
    <>
      <div  className='flex flex-row flex-wrap'>
        {
          items.map((item,i) => {
            return (
              <>

                <Card style={{ width: '18rem' }} key={item.item.id} >
                  <Card.Img variant="top" src={item.item.image} />
                  <Card.Body>
                    <Card.Title>{item?.title}</Card.Title>
                    <Card.Text>
                    {item.item.description}
                    </Card.Text>
                    <Button variant="primary" onClick={()=>dispatch(removeToCart(item.item.id))} >remove to cart</Button>
                  </Card.Body>
                  <h1>{item.qty}</h1>
                  <button className='p-2 text-3xl bg-gray-700' onClick={()=>dispatch(increment(item))}>+</button>
                  <button className='p-2 text-3xl bg-gray-700' onClick={()=>dispatch(decrement(item))}>-</button>
                </Card>
              </>
            )
          })
        }
      </div>
    </>
  )
}



export default AddItem
