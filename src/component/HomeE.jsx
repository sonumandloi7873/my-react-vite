import React, { useEffect, useState } from 'react'
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import { useDispatch, useSelector } from 'react-redux'
import { addToCart, searchToCart } from '../redux/slice';
import { NavLink, useNavigate } from 'react-router-dom';

function HomeE() {

  const dispatch = useDispatch()
  const naviagte = useNavigate()
  const items = useSelector((state) => state.data.apiList)

  const [search,setSearch] = useState("")

  function handlerSearch(e){
    setSearch(e.target.value)
  }

  useEffect(()=>{
    dispatch(searchToCart(search))
  },[search])

  // console.log("search", search)
  // console.log("item", items)

  return (
    <>
      <Navbar expand="lg" className="bg-slate-300">
        <Container fluid>
          <Navbar.Brand href="#">Navbar scroll</Navbar.Brand>
          <Navbar.Toggle aria-controls="navbarScroll" />
          <Navbar.Collapse id="navbarScroll">
            <Nav
              className="me-auto my-2 my-lg-0"
              style={{ maxHeight: '100px' }}
              navbarScroll
            >
              <Nav.Link href="/">Home</Nav.Link>
              <NavLink to="/AddItem">Product</NavLink>
            </Nav>
            <Form className="d-flex">
              {/* <Form.Control
                type="search"
                placeholder="Search"
                className="me-2"
                aria-label="Search"
                onChange={handlerSearch}
              /> */}
              <input type="text"  onChange={handlerSearch} value={search} />
              <Button variant="outline-success">Search</Button>
            </Form>
          </Navbar.Collapse>
        </Container>
      </Navbar>

      <div className='flex flex-row flex-wrap'>
        {
          items.map((item, i) => {
            return (
              <>

                <Card style={{ width: '18rem' }} key={item.id}  >
                  <Card.Img variant="top" src={item.image} />
                  <Card.Body>
                    <Card.Title>{item?.title}</Card.Title>
                    <Card.Text>
                      {item.description}
                    </Card.Text>
                    <Button variant="primary" onClick={() => dispatch(addToCart({item,qty:1}))} >add to cart</Button>
                    <Button variant="primary" onClick={()=>naviagte(`/Deatiles/${item.id}`)} >Deatiles</Button>
                  </Card.Body>
                </Card>
              </>
            )
          })
        }
      </div>
    </>
  )
}



export default HomeE
