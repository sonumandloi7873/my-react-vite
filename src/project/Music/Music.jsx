import React, { useEffect, useRef, useState } from 'react'
import './music.css'
import { musicData } from './musicData'
import { IoPlayBack, IoPlayForward, IoPlayCircle } from "react-icons/io5";
import { FaCirclePause } from "react-icons/fa6";

function Music() {

  const[currentSong,setCurrentSong] = useState(0)
  const [isPlaying,setIsPlaying] = useState(false)

  const [nextSong, setNextSong] = useState(0)

  const audio = useRef()

  useEffect(()=>{
    setNextSong((currentSong + 1 > musicData.length - 1) ? 0 : currentSong + 1)
  },[currentSong])


 useEffect(()=>{
  if(isPlaying){
    audio.current.play()
  }else{
    audio.current.pause()
  }
 })

 function forwardSong(){
  if(currentSong == musicData.length - 1){
    setCurrentSong(0)
    setIsPlaying(true)
  }else{
    setCurrentSong(currentSong + 1)
    setIsPlaying(true)
  }
 }

function backwardSong(){
  if(currentSong == 0){
    setCurrentSong(musicData.length - 1)
    setIsPlaying(true)
  }else{
    setCurrentSong(currentSong - 1)
    setIsPlaying(true)


  }
}

  return (
    <div className='container_music'>
      <div className='Music_cart'>
        <div className='image'>
          <img src={musicData[currentSong].img_src}  alt='' />
        </div>
        <div className='title'>
          <h4>{musicData[currentSong].title}</h4>
          <h5>{musicData[currentSong].artist}</h5>
        </div>
        <div className='music_btn'>
          <button onClick={backwardSong}><IoPlayBack/></button>
          <button onClick={()=>setIsPlaying(!isPlaying)}>{isPlaying ? <FaCirclePause/> : <IoPlayCircle/>}</button>
          <button onClick={forwardSong}><IoPlayForward/></button>
        </div>
        <div className='audio'>
          <audio src={musicData[currentSong]. audio_src} ref={audio} controls></audio>
        </div>
        <div>
          <h4>next song : {musicData[nextSong].title}</h4>
        </div>
      </div>
    </div>
  )
}

export default Music
