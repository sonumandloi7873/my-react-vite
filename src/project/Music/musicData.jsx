// audio 
import Apna_Bana_Le from '../../../../../../../../../music/Apna_Bana_Le.mp3'
import Dekhha_Tene from '../../../../../../../../../music/Dekhha_Tene.mp3'
import Hua_Main from '../../../../../../../../../music/Hua_Main.mp3'
import Mere_Mehboob_Mere_Sanam from '../../../../../../../../../music/Mere_Mehboob_Mere_Sanam.mp3'
import Phir_Aur_Kya_Chahiye from '../../../../../../../../../music/Phir_Aur_Kya_Chahiye.mp3'



// image 
import mahi from '../../../../../../Desktop/image/mahi.jpg'
import apan_bane from '../../../../../../Desktop/image/apan_bane.jpg'
import Bad_Newz from '../../../../../../Desktop/image/Bad_Newz.jpeg'
import hua_main from '../../../../../../Desktop/image/hua_main.jpg'
import zara_aur_kya_chahiya from '../../../../../../Desktop/image/zara_aur_kya_ chahiya.jpg'





export const musicData = [
  {
    img_src:  Bad_Newz,
    audio_src: Mere_Mehboob_Mere_Sanam ,
    title:'Mere Mehboob Mere Sanam',
    artist:'Udit Narayan, Alka Yagnik'
  },
  {
    img_src:  apan_bane,
    audio_src: Apna_Bana_Le,
    title:'Apna Bana Le',
    artist:'Arijit Singh'
  },
  {
    img_src:  zara_aur_kya_chahiya,
    audio_src: Phir_Aur_Kya_Chahiye ,
    title:'Phir_Aur_Kya_Chahiye',
    artist:'Arijit Singh'
  },  {
    img_src:  hua_main,
    audio_src: Hua_Main,
    title:'Hua Main ',
    artist:'Raghav Chaitanya, Pritam '
  },
  {
    img_src: mahi,
    audio_src: Dekhha_Tene,
    title:'Dekhha Tenu ',
    artist:'Udit Narayan'
  }
]