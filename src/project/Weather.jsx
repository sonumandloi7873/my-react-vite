import axios from 'axios'
import React, { useEffect, useState } from 'react'
import './weather.css'

function Weather() {

  const apiKey = "38895d1199c4161833a308f65a79fde9"

  const [city,setCity] = useState("pune")
  const [data,setData] = useState([])

  useEffect(()=>{
    const ApiCall = async()=>{
      let result  = await axios.get(`https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${apiKey}`)
      setData(result.data)
    }
    ApiCall()
  },[])

console.log("data",data)

  return (
    <div className='container'>
      <div className='weather'>
        <div className='inputBOx'>
          <input type="text" onChange={(e)=>setCity(e.target.value)} />
        </div>
        <div className='Deatiles'>
          <h3>tem: {data.main.temp}</h3>
          <h3>max tem: {data.main.temp_max}</h3>
          <h3>min tem: {data.main.temp_min}</h3>
          <h3>city name : {data.name}</h3>
        </div>
      </div>
    </div>
  )
}

export default Weather
