import React, { useState } from 'react'

export function AddDataInArray() {

  const [data, setData] = useState("")
  const [arr, setArr] = useState([])

  function addData() {
    setArr([...arr, data])
    setData("")

  }

  function remove(i){
    arr.splice(i,1)
    setArr([...arr])
  }

console.log(arr)
  return (
    <>

      <input type="text" value={data} onChange={(e) => setData(e.target.value)} />
      {
        data.length > 0 ? <button onClick={addData}>submit</button> : <button>submit</button>
      }
{
        arr.map((item,i) => {
          return (
            <>
              <div className='box' key={i}>
                <h2>{item}</h2>
                <i onClick={()=>remove(i)} class="fa-solid fa-xmark"></i>
              </div>
              </>
          )
        })
        } 
      <div>
      </div>
    </>
  )
}

