import React, { useState } from 'react'

function Count() {

  const [count,setCount] = useState(0)

  function sum(){
    setCount(count + 1)
  }
  function sub(){
    setCount(count - 1)
  }

  return (
    <>
    <h1>Count</h1>
    <h2>{count}</h2>

    <button onClick={sum} >Add</button>
    <button onClick={sub} >sub</button>
      
    </>
  )
}

export default Count
