import React, { useState } from "react";

 function Event(){

  const [data,setData] = useState("")
  const [print,setPrint] = useState(false)
  
  console.log("print =>",print)
  console.log("data =>",data)
  
  return(
  <>
    <div>my vite bundler use</div>
    <input type="text" onChange={(e)=>setData(e.target.value)} />

      {
        print ?  <h2>{data}</h2>   : null
      }
    
    <button onClick={()=>setPrint(true)}>print</button>
    
  </>

  )
}

export default Event