import React, { useEffect } from 'react'
import { Button } from 'react-bootstrap'
import Card from 'react-bootstrap/Card';
import axios from 'axios'

function Map() {

  const data = ["a","b","c","d","e"]
  const aaa = [1,2,3,4,5]
  

    // data.map((item)=>console.log(item) )

    // data.forEach((item)=>console.log(item) )

    const obj = async() =>{
      const result = await axios.get("https://fakestoreapi.com/products")
      console.log("result",result.data)
    }

    useEffect(()=>{
      obj()
    },[])

  return (
    <div>

      <h1>map and loop</h1>


      {
        data.map((item,i)=>{
          return(
            <>
            <Card style={{ width: '18rem' }} key={i}>
                <Card.Img variant="top" src="holder.js/100px180" />
                <Card.Body>
                  <Card.Title>Card Title</Card.Title>
                  <Card.Text>
                   {item}
                  </Card.Text>
                  <Button variant="primary">Go somewhere</Button>
                </Card.Body>
              </Card>
            <Button variant="danger">click me</Button>
            </>
          )
        })
      }

</div>
  )
}
export default Map
