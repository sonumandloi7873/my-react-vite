import React, { useRef } from "react";

export const RefUse = () =>{

  const inputRef = useRef(null)

  function refInput() {
    console.log("inputRef",inputRef)
    inputRef.current.value ="1000"
    inputRef.current.style.color ="red"
    inputRef.current.focus()

  }

  return(
    <>
    <input type="text"  ref={inputRef} />
    <button onClick={()=>refInput()}>click me</button>
        

    </>
  )
} 
