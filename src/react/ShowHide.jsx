import React, { useState } from 'react'

function ShowHide() {

  const [name,setName] = useState("sonu")
  const [print,setPrint] = useState(false)

  return (
    <div>
      {
        print ? <h2>{name}</h2>  :  null
      }
      
      <button onClick={()=>setPrint(true)}>Show</button>
      <button onClick={()=>setPrint(false)}>Hide</button>

      <button onClick={()=>setPrint(!print)}>toggle</button>
      
    </div>
  )
}

export default ShowHide


