import React from "react";
import './Style.css'
import style from './custom.module.css'


export const Styles = () => {
  return(
    <>

    <h1 style={{color:"red"}}>Inline Style </h1>
    <h1 className="data">Inline Style </h1>
    <h1 className={style.hello}>Inline Style </h1>
    
    </>

  )
}
