import React from 'react'
import Table from 'react-bootstrap/Table'

function Tables() {


  const data = [

    {
      name:"a",
      add:"abc",
      mob:"99xxxxx"
    },

    {
      name:"b",
      add:"def",
      mob:"88xxxxx"
    },

    {
      name:"c",
      add:"abc",
      mob:"99xxxxx"
    },
    
    {
      name:"a",
      add:"ghi",
      mob:"77xxxxx"
    }
  ]

  return (
    <div>
    
            <Table striped bordered  hover>
              <tr>
                <th>name</th>
                <th>add</th>
                <th>mob</th>
              </tr>
              {
                data.map((item,i)=>{
                  return(
                    <>
                      <tr key={i}>
                        <td>{item.name}</td>
                        <td>{item.add}</td>
                        <td>{item.mob}</td>
                      </tr>
              </>
          )
        })
      }
            </Table>
           
    </div>
  )
}

export default Tables
