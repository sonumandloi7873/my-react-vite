import React, { useState } from 'react'

function Input() {

  const [inputName,setInputName] = useState("")
  const [print,setPrint] = useState(false)

  function handInput(e){
    setInputName(e.target.value)
  }
function handlerPrint(){
  setPrint(true)
}
function handlerHide(){
  setPrint(false)
}

  return (
    <div>
      <input type="text" onChange={handInput} />
      <button onClick={handlerPrint}>show me</button>
      <button onClick={handlerHide}>hide me</button>
      <button onClick={()=>setPrint(!print)}>toggle</button>
      {
        print  ? <h2>{inputName}</h2> : null 
      }
    </div>
  )
}

export default Input
