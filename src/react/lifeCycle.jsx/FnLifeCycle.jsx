import React, { useEffect, useState } from 'react'
import Count from '../Count'

function FnLifeCycle() {

  const [name,setName] = useState(0)
  const [count,setCount ] = useState(5)

  useEffect(()=>{
  console.log("useefect")
  },[count])
  

  return (
    <div>
      <h1>{name}</h1>
      <h1>{count}</h1>
      <button onClick={()=>setName(name + 1)} >clicke +1</button>
      <button onClick={()=>setCount(count + 5)} >clicke +5</button>
    </div>
  )
}

export default FnLifeCycle
