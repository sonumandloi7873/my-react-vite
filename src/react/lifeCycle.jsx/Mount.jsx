import React from 'react'

class Mount extends React.Component{
    constructor(){
      super();
      this.state={
        data:"sonu"
      }
      console.log("super")

    }

    componentDidMount(){
      console.log("mount")
    }
    componentDidUpdate(preState){
      console.log("update")   
     }
     shouldComponentUpdate(){
      return false
     }

  render(){
    console.log("render")

    return(
      <>
      <div>{
        this.state.data
      }</div>

      <button onClick={()=>this.setState({data:"mandloi"})}>click me</button>
      </>
    )
  }
}

export default Mount
