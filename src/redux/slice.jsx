import { createSlice } from "@reduxjs/toolkit";

const slice = createSlice ({

  name:"Project",
  initialState:{
    apiList:[],
    addCartList:[]
  } ,
  reducers:{

    apiStore : (state,action) =>{
      state.apiList = action.payload

    },
    addToCart:(state,action)=>{
      const exteradd = state.addCartList.find((item)=>item?.item?.id === action.payload?.item.id);
      // console.log(action.payload);
      if(exteradd){
          state.addCartList=state.addCartList.map((item)=>item?.item.id === action.payload?.item.id ? {...item,qty:item.qty + 1} : item)
      }else{
      state.addCartList.push(action.payload);
      }
    },
    removeToCart:(state,action)=>{
      state.addCartList =  state.addCartList.filter((item)=>item.item.id !== action.payload)
    },
    searchToCart:(state,action)=>{
      state.apiList = state.apiList.filter((item)=>item.title.toLowerCase().includes(action.payload.toLowerCase()))
    },

    increment:(state,action)=>{
      state.addCartList = state.addCartList.map((item)=>item?.item.id === action.payload.item.id ? {...item,qty:item.qty + 1}:item)
      // console.log(action.payload);
      
    },

    decrement:(state,action)=>{
       state.addCartList = state.addCartList.map((item)=>item?.item.id === action.payload.item.id ?  (item.qty > 1 ? {...item,qty:item.qty - 1}: item): item)
    }
    
  }

})

export const {apiStore,addToCart,removeToCart,searchToCart,increment,decrement} = slice.actions

export default slice.reducer