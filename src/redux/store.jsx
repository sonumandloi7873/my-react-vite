import { configureStore } from "@reduxjs/toolkit";
import homeslice from "./slice";

const store = configureStore({
  reducer :{
   data : homeslice
   
  }
})

export default store