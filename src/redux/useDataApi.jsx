
import axios from 'axios'
import React, { useEffect } from 'react'
import { useDispatch } from 'react-redux'
import { apiStore } from './slice'

function useDataApi() {

  const dispatch = useDispatch()

  const apiCall = async()=>{
    const apiData = await axios.get("https://fakestoreapi.com/products")
    // console.log("apiData",apiData.data)

    dispatch(apiStore(apiData.data))
  }
  useEffect(()=>{
    apiCall()
  },[])
}

export default useDataApi
